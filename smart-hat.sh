#!/bin/bash




# 
# The purpose is to check at each end of period if there is
# a new version of the softxare, to build it, and to start it
# instead of the previous one. This software will enable
# prod usage of RC version of a sofware without manual
# intervention.
#


#########################################################
#                                                       #
#          Script Proposal for continuous               #
#                    integration                        #
#                                                       #
#            J.G -- linus.belap@gmail.com               #
#                                                       #
#########################################################






PROGRAM_NAME="mybinary"
PROGRAM_REPO="../repobinary/"
PROGRAM_PID=0
PROGRAM_BRANCH="master"
PROGRAM_BUILD_COMMAND=""
TIME_BETWEEN_TWO_UPDATE=10


if [ -f program_settings ]; then
    . ./program_settings
else
    echo "$0 You can add the program_settings file to set defaults parameters"
fi


function repo_check
{
    cd $PROGRAM_REPO

    LOCAL=$(git rev-parse @)
    REMOTE=$(git rev-parse @{u})
    BASE=$(git merge-base @ @{u})
    
    if [ $LOCAL = $REMOTE ]; then
        echo "Up-to-date"
        return 0
    elif [ $LOCAL = $BASE ]; then
        echo "Need to pull"
        return 1
    elif [ $REMOTE = $BASE ]; then
        echo "Need to push"
        return 2
    else
        echo "Diverged"
        return 3
    fi
    cd -
}


function program_stop
{
    if [[ $PROGRAM_PID != 0 ]]; then
        kill -15 $PROGRAM_PID
    fi

    PROGRAM_PID=0

    # Add a waiting function ? TODO
}


function repo_update
{
    cd $PROGRAM_REPO
    git pull -u origin $PROGRAM_BRANCH
    cd -
}


function program_build
{
    if [[ $PROGRAM_BUILD_COMMAND == "" ]]; then
        echo "$0: no build command provided, thus, reexecute binary as is"
    else
        cd $PROGRAM_REPO
        # Cannot execute command stored in a variable, set it it manually here
        if [[ $PROGRAM_BUILD_COMMAND == "make all" ]]; then
            make clean
            make all
        else
            echo "$0: need to add this command : $PROGRAM_BUILD_COMMAND in the robot"
            echo "$0: try that by default : make "$PROGRAM_NAME
            make $PROGRAM_NAME
        fi

        cd -
    fi
}



#
# Note that it is imposible to start it as a daemon with a redirection of traces to
# a file - especially for stderr
#
function program_start
{
    if [[ $PROGRAM_PID == 0 ]]; then
        cd $PROGRAM_REPO
        ./$PROGRAM_NAME &
        PROGRAM_PID=$!
        echo "$0: PID of the current instance is $PROGRAM_PID"
        cd -
    else
        echo "$0: error, PID != 0, thus another instance of the program is running"
    fi
}


trap ctrl_c INT
function ctrl_c() {
    echo "** Trapped CTRL-C"

    program_stop
    exit 1
}





program_start


while [ true ]
do

    repo_check
    repo_status=$?
    
    
    case $repo_status in
        0)
            echo "$0: already up to date - nothing to do"
            ;;
        1)
            program_stop
            repo_update
            program_build
            program_start
            ;;
    
        *)
            echo "Anormal return of the repository check - abort"
            return 1
            ;;
    esac

    sleep $TIME_BETWEEN_TWO_UPDATE
done    
